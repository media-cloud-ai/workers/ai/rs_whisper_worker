# rs-whisper-worker

A Rust worker based on [`whisper-rs`](https://github.com/tazz4843/whisper-rs), providing bindings to [`whisper.cpp`](https://github.com/ggerganov/whisper.cpp).

## Usage

The model is provided to the worker via the environment variable `WHISPER_MODEL_PATH`.
