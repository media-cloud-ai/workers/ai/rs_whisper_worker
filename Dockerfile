FROM rust:1.76.0 as builder

ADD . /src 
WORKDIR /src

RUN apt-get update && \
    apt-get install -y \
        cmake \
        libclang-dev \
        libssl-dev \
        && \
    cargo build --verbose --release && \
    cargo install --path .

FROM debian:bookworm
COPY --from=builder /usr/local/cargo/bin/rs_whisper_worker /usr/bin

RUN apt update && \
    apt install -y \
        openssl \
        ca-certificates

ENV AMQP_QUEUE job_whisper
CMD rs_whisper_worker
