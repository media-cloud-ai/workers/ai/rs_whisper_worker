use crate::error::{
    processing_hound_error, processing_io_error, processing_string_error, processing_whisper_error,
};
use crate::format::{json::whisper_to_json, text::whisper_to_text};
use crate::WorkerParameters;
use mcai_worker_sdk::{job::JobResult, prelude::*};
use std::fs::File;
use std::io::Write;
use whisper_rs::{get_lang_str, FullParams, SamplingStrategy, WhisperContext};

pub fn process(
    _channel: Option<McaiChannel>,
    parameters: WorkerParameters,
    job_result: JobResult,
    context: &WhisperContext,
) -> Result<JobResult> {
    // Create a state
    let mut state = context
        .create_state()
        .map_err(|e| processing_whisper_error(&job_result, e))?;

    // Create a params object for running the model.
    // The number of past samples to consider defaults to 0.
    let mut params = FullParams::new(SamplingStrategy::Greedy { best_of: 0 });

    // Edit params as needed.
    // Set the number of threads to use.
    params.set_n_threads(4);
    // Enable translation.
    params.set_translate(false);
    // Set the language to transcribe.
    if let Some(lang) = &parameters.language {
        params.set_language(Some(lang));
    } else {
        warn!("No language specified, auto-detecting language.");
        params.set_language(None);
    };
    params.set_max_len(1);
    params.set_split_on_word(true);
    params.set_token_timestamps(true);
    // Disable anything that prints to stdout.
    params.set_print_special(false);
    params.set_print_progress(false);
    params.set_print_realtime(false);
    params.set_print_timestamps(true);

    // Open the audio file.
    let reader = hound::WavReader::open(parameters.source_path)
        .map_err(|e| processing_hound_error(&job_result, e))?;

    #[allow(unused_variables)]
    let hound::WavSpec {
        channels,
        sample_rate,
        bits_per_sample,
        ..
    } = reader.spec();

    // Convert the audio to floating point samples.
    let samples: Vec<i16> = reader
        .into_samples::<i16>()
        .map(|x| x.expect("Invalid sample"))
        .collect();
    let mut audio = vec![0.0f32; samples.len()];
    whisper_rs::convert_integer_to_float_audio(&samples, &mut audio)
        .map_err(|e| processing_whisper_error(&job_result, e))?;

    // Convert audio to 16KHz mono f32 samples, as required by the model.
    // These utilities are provided for convenience, but can be replaced with custom conversion logic.
    // SIMD variants of these functions are also available on nightly Rust (see the docs).
    if channels == 2 {
        audio = whisper_rs::convert_stereo_to_mono_audio(&audio).unwrap();
    } else if channels != 1 {
        return Err(processing_string_error(
            &job_result,
            "Error: >2 channels unsupported",
        ));
    }

    if sample_rate != 16000 {
        return Err(processing_string_error(
            &job_result,
            "Error: Sample rate must be 16KH",
        ));
    }

    // Run the model.
    state
        .full(params, &audio[..])
        .map_err(|e| processing_whisper_error(&job_result, e))?;

    let language = state
        .full_lang_id_from_state()
        .map_err(|e| processing_whisper_error(&job_result, e))?;

    let lang: &str = match get_lang_str(language) {
        Some(lang) => {
            info!("Language provided or detected: {}", lang);
            lang
        }
        _ => {
            warn!("No language provided nor detected");
            "None"
        }
    };

    let content = match &parameters.output_format.as_ref().map(String::as_ref) {
        Some("json") => {
            let transcript = whisper_to_json(&state, &job_result)?;
            serde_json::to_string(&transcript)?
        }
        _ => {
            warn!("Unknown output, falling back to text output.");
            whisper_to_text(&state, &job_result)?
        }
    };

    // Create a file to write the transcript to.
    let mut file = File::create(parameters.destination_path)
        .map_err(|e| processing_io_error(&job_result, e))?;

    // Write the segment information to the file.
    file.write_all(content.as_bytes())
        .map_err(|e| processing_io_error(&job_result, e))?;

    Ok(job_result
        .with_status(JobStatus::Completed)
        .with_parameters(&mut vec![Parameter {
            id: "lang".to_string(),
            kind: "string".to_string(),
            value: Some(json!(lang)),
            default: None,
            store: None,
        }]))
}
