use crate::error::processing_whisper_error;
use mcai_worker_sdk::{prelude::JobResult, prelude::*};
use whisper_rs::WhisperState;

pub fn whisper_to_text(state: &WhisperState, job_result: &JobResult) -> Result<String> {
    // Iterate through the segments of the transcript.
    let num_segments = state
        .full_n_segments()
        .map_err(|e| processing_whisper_error(job_result, e))?;

    let mut text = "".to_string();

    for i in 0..num_segments {
        // Get the transcribed text and timestamps for the current segment.
        let segment = state
            .full_get_segment_text(i)
            .map_err(|e| processing_whisper_error(job_result, e))?;

        text = format!("{} {}", text, segment);
    }
    Ok(text)
}
