use crate::error::processing_whisper_error;
use mcai_worker_sdk::{prelude::JobResult, prelude::*};
use whisper_rs::WhisperState;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Word {
    pub content: String,
    pub speaker_id: u64,
    pub start_time: f64,
    pub end_time: f64,
    pub confidence: f64,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Speaker {
    pub gender: String,
    pub id: u64,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Json {
    pub speakers: Vec<Speaker>,
    pub words: Vec<Word>,
    pub text: String,
    pub provider: String,
}

pub fn whisper_to_json(state: &WhisperState, job_result: &JobResult) -> Result<Json> {
    // Iterate through the segments of the transcript.
    let num_segments = state
        .full_n_segments()
        .map_err(|e| processing_whisper_error(job_result, e))?;

    let mut words: Vec<Word> = vec![];
    let mut text = "".to_string();
    let mut speaker_id: u64 = 0;
    let mut speakers: Vec<Speaker> = vec![];

    for i in 0..num_segments {
        // Get the transcribed text and timestamps for the current segment.
        let segment = state
            .full_get_segment_text(i)
            .map_err(|e| processing_whisper_error(job_result, e))?;
        let start_timestamp = state
            .full_get_segment_t0(i)
            .map_err(|e| processing_whisper_error(job_result, e))?;
        let end_timestamp = state
            .full_get_segment_t1(i)
            .map_err(|e| processing_whisper_error(job_result, e))?;

        if segment.is_empty() || segment == " ..." {
            continue;
        }

        let mut confidence: f64 = 0.0;
        let token_number = state.full_n_tokens(i).unwrap_or(0);
        for j in 0..token_number {
            let prob = state.full_get_token_prob(i, j).unwrap_or(0.0);
            confidence += prob as f64;
            debug!("{:?}", state.full_get_token_text(i, j));
            debug!("{:?}", state.full_get_token_prob(i, j));
        }
        confidence /= token_number as f64;

        debug!("[{} - {}]: {}", start_timestamp, end_timestamp, segment);

        if segment.starts_with(" -") {
            speaker_id += 1;
            speakers.push(Speaker {
                gender: "N/A".to_owned(),
                id: speaker_id,
            });
        }

        words.push(Word {
            content: segment.clone(),
            speaker_id,
            start_time: start_timestamp as f64,
            end_time: end_timestamp as f64,
            confidence,
        });

        text = format!("{}{}", text, segment);
    }
    Ok(Json {
        speakers,
        words,
        text,
        provider: "whisper.cpp".to_string(),
    })
}
