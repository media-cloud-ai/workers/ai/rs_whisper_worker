use mcai_worker_sdk::{job::JobResult, prelude::*, MessageError};
use whisper_rs::WhisperError;

pub fn processing_whisper_error(job_result: &JobResult, error: WhisperError) -> MessageError {
    processing_error(job_result, &format!("Error: {}", error))
}

pub fn processing_hound_error(job_result: &JobResult, error: hound::Error) -> MessageError {
    processing_error(job_result, &format!("Error: {}", error))
}

pub fn processing_io_error(job_result: &JobResult, error: std::io::Error) -> MessageError {
    processing_error(job_result, &format!("Error: {}", error))
}

pub fn processing_string_error(job_result: &JobResult, error: &str) -> MessageError {
    processing_error(job_result, error)
}

pub fn processing_error(job_result: &JobResult, error: &str) -> MessageError {
    MessageError::ProcessingError(
        job_result
            .clone()
            .with_status(JobStatus::Error)
            .with_message(error),
    )
}
