#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

mod error;
use error::processing_error;
mod format;
mod whisper;

use mcai_worker_sdk::{default_rust_mcai_worker_description, job::JobResult, prelude::*};
use std::sync::{Arc, Mutex};
use whisper_rs::{WhisperContext, WhisperContextParameters};

default_rust_mcai_worker_description!();

#[derive(Debug, Default)]
struct McaiRustWorker {}

#[derive(Debug, Clone, Default)]
#[allow(dead_code)]
struct WhisperEvent {
    context: Arc<Mutex<std::cell::RefCell<Option<WhisperContext>>>>,
    model_path: Arc<Mutex<std::cell::RefCell<Option<String>>>>,
}

#[derive(Debug, Clone, Deserialize, JsonSchema)]
#[allow(dead_code)]
pub struct WorkerParameters {
    /// # Language
    /// Transcription language
    language: Option<String>,
    /// # Output Format
    /// Output Format for transcription between text and json
    output_format: Option<String>,
    /// # Model Path
    /// Model path for Whisper context
    model_path: Option<String>,
    destination_path: String,
    source_path: String,
}

impl WhisperEvent {
    fn load_model(self, model_path: Option<String>) -> Result<()> {
        if model_path != *(*self.model_path.lock().unwrap()).borrow() && model_path.is_some() {
            // Load a context and model.
            let whisper_params = WhisperContextParameters { use_gpu: is_gpu() };
            match WhisperContext::new_with_params(model_path.as_ref().unwrap(), whisper_params) {
                Ok(context) => {
                    info!("New model loaded from {:?}", model_path);
                    (*self.context.lock().unwrap()).replace(Some(context));
                    (*self.model_path.lock().unwrap()).replace(model_path);
                    Ok(())
                }
                Err(error) => Err(MessageError::RuntimeError(format!("Error: {}", error))),
            }
        } else {
            info!(
                "No new model to load. Actual model from: {:?}",
                *(*self.model_path.lock().unwrap()).borrow()
            );
            Ok(())
        }
    }
}

impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for WhisperEvent {
    fn init(&mut self) -> Result<()> {
        self.context = Arc::new(Mutex::new(std::cell::RefCell::new(None)));
        self.model_path = Arc::new(Mutex::new(std::cell::RefCell::new(None)));
        match std::env::var("WHISPER_MODEL_PATH") {
            Ok(path) => self.clone().load_model(Some(path)),
            Err(error) => Err(MessageError::RuntimeError(format!("Error: {}", error))),
        }
    }

    fn process(
        &self,
        channel: Option<McaiChannel>,
        parameters: WorkerParameters,
        job_result: JobResult,
    ) -> Result<JobResult> {
        self.clone()
            .load_model(parameters.model_path.clone())
            .map_err(|e| processing_error(&job_result, &e.to_string()))?;

        whisper::process(
            channel,
            parameters,
            job_result,
            (*self.context.lock().unwrap()).borrow().as_ref().unwrap(),
        )
    }
}

fn is_gpu() -> bool {
    std::env::var("WHISPER_GPU")
        .unwrap_or_else(|_| "false".to_owned())
        .parse()
        .unwrap()
}

fn main() {
    let worker = WhisperEvent::default();
    start_worker(worker);
}
